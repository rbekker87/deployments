#!/bin/bash

REPO_SOURCE="https://gitlab.com/rbekker87"
YUM=`which yum 2>/dev/null | wc -l `
APT=`which apt 2>/dev/null| wc -l `

printHelp() {
    echo '
Deploy Client still in Progress
'
}

listAllScripts() {
    echo '

   +-------------+--------------+------------+
   | Application |    Distro    | Param Name |
   +-------------+--------------+------------+
   |- Logstash   |  RHEL/Debian |   logstash |
   +-------------+--------------+------------+

    '
}


installLogstash() {
    curl -Ss $REPO_SOURCE/scripts/raw/master/setup-logstash.sh
}

raiseException() {
    echo 'ERROR: Still in progress'
}

if [ -z "$1" ] || [ "$1" = "help" ] ;
    then
        printHelp
        exit 0

    elif [ "$1" = "--list" ] ;
        then
            listAllScripts
            exit 0

    elif [ "$1" = "--install" ] && [ "$2" = "logstash" ] ;
        then
            installLogstash

    else
       raiseException

fi
