#!/bin/bash

INSTALL_LOG="./install-$(date +%s).log"
STDOUT_LOG="./install-$(date +%s).stdout.log"
STDERR_LOG="./install-$(date +%s).stderr.log"

# dependencies
yum install openssl openssl-devel -y

# set root password
export DBPASS=$(openssl rand -hex 10)
echo "MySQL Root Password: $DBPASS " >> $INSTALL_LOG

# disable selinux
setenforce 0
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config

# updates
yum update -y 
yum install epel-release -y
yum install yum-priorities -y
yum groupinstall 'Development Tools' -y

# dependencies
yum install wget ntp httpd mod_ssl mysql-server php php-mysql php-mbstring \
dovecot dovecot-mysql postfix amavisd-new spamassassin clamav clamd unzip bzip2 \
unrar perl-DBD-mysql php php-devel php-gd php-imap php-ldap php-mysql php-odbc \
php-pear php-xml php-xmlrpc php-pecl-apc php-mbstring php-mcrypt php-mssql \
php-snmp php-soap php-tidy curl curl-devel perl-libwww-perl ImageMagick libxml2 \
libxml2-devel mod_fcgid php-cli httpd-devel httpd-devel ruby ruby-devel \
mod_python openssl perl-DateTime-Format-HTTP perl-DateTime-Format-Builder -y

# update spamassassin
sa-update

# php configuration
sed -i 's/;error_reporting = E_ALL & ~E_DEPRECATED/error_reporting = E_ALL & ~E_NOTICE/' /etc/php.ini
sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=1/' /etc/php.ini

# enable services 
chkconfig --levels 235 dovecot on
chkconfig --levels 235 mysqld on
chkconfig --levels 235 postfix on
chkconfig --levels 235 httpd on
chkconfig --levels 235 amavisd on
chkconfig --del clamd
chkconfig --levels 235 clamd.amavisd on

# start services
/etc/init.d/dovecot start
/etc/init.d/mysqld start
/etc/init.d/postfix restart
/etc/init.d/httpd restart

# anti-virus definitions
/usr/bin/freshclam

